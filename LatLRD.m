close all; clear ; clc

%%
test         = false;
QuickBird    = true;    % sub-urban
Pleaides     = false;   % sub-urban 
WorldView2   = false;   % urban
%% Datasets
if (QuickBird)
    pan = double(imread('PAN_LR_QB_320.tif')); 
    msr = double(imread('MS_QB_320.tif'));
    % Coefficients beta1 & beta2 to fuse "the latent low rank matrices"
    beta1 = 1.7;  
    beta2 = 1.0;
elseif  (Pleaides)
    pan = double(imread('PAN_LR_PL_320.tif')); 
    msr = double(imread('MS_PL_320.tif'));
    % Coefficients beta1 & beta2 to fuse "the latent low rank matrices"
    beta1 = 1.0;  % min_ergas
    beta2 = 0.6;
else %(WorldView2)
    load ('PAN_LR_WV2_256_data2.mat')
    load ('MS_WV2_256_data2.mat')
    % Coefficients beta1 & beta2 to fuse "the latent low rank matrices"
    beta1 = 2.0;
    beta2 = 0.1;
    pan = double(PAN_LR); 
    msr = double(MS);
end

%% Data normalization
pann    = normalize1(pan);
msrn    = normalize1(msr);
% pann = pan;
% msrn = msr;
%
bands = size(msrn,3);
%% Visualization
figure,
subplot(122)
J1(:,:,1) = imadjust(msrn(:,:,3));
J1(:,:,2) = imadjust(msrn(:,:,2));
J1(:,:,3) = imadjust(msrn(:,:,1));
axis off;
imshow(J1);set(gcf,'PaperPositionMode','auto');title('Reference Image')
subplot(121)
J2 = imadjust(pann);
imshow(J2);axis off,colormap gray,title('PAN Image')
%
lambda =  0.9; 
%% PAN decomposition
X1 = pann;
[Z1,L1,E1] = latent_lrr(X1,lambda);
disp(['LatLRD decomposition of PAN image ']);
I_lrr1 = X1*Z1;
I_saliency1 = L1*X1;
% figure,imshow(Z1,[])  
% figure,imshow(L1,[])   
% figure,imshow(E1,[])    
%
I_lrr1 = max(I_lrr1,0);
I_lrr1 = min(I_lrr1,1);
I_saliency1 = max(I_saliency1,0);
I_saliency1= min(I_saliency1,1);
I_e1 = E1;

% figure,imshow(I_lrr1,[])  
% figure,imshow(I_saliency1,[])  
figure,imshow(I_lrr1,[]); title('Low-rank component')
figure,imshow(I_saliency1,[]); title('Saliency component') 
figure,imshow(I_e1,[]); title('Sparse component') 

