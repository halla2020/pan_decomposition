function RES=normalize1(data)

% This function is to NORMALIZE the data. 
% The data will be in the interval 0-255 (gray level) and pixel value has
% been rounded to an integer.

data=double(data);
max_data=max(data(:));
min_data=min(data(:));
if (max_data==0 && min_data==0)
    RES=data;
else
    RES=(data-min_data)/(max_data-min_data);
end


